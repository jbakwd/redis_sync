#include "sync.h"
#include "log.h"
#include "util.h"
#include "thread_pool.h"
#include "msg.h"

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>  //sleep
#include <errno.h>
#include <sys/time.h> /* for struct timeval */


static pool_context_t  *pcxt = NULL;

static tpool_t **pools = NULL;
static int pool_num = 0;
static int timeout_secs = 1;
static struct timeval timeout_tv;

/**
** 初始化线程上下文数据
**/
static void* sync_thd_ctx_init_func(void * pool_ctx_p, void ** thd_ctx_pp){
    pool_context_t * pool_ctx = (pool_context_t *)pool_ctx_p;
    thread_context_t * thd_ctx = (thread_context_t *)(*thd_ctx_pp);
    if(thd_ctx==NULL){
        thd_ctx = (thread_context_t *)calloc(1,sizeof(thread_context_t));
        if(!thd_ctx){
            ERROR("sync_thd_ctx_init_func failed");
            return NULL;
        }
        thd_ctx->conn = redisConnectWithTimeout(pool_ctx->ip_buf, pool_ctx->port, timeout_tv); 
        if ( thd_ctx->conn->err) {  
            redisFree(thd_ctx->conn);  
            free(thd_ctx);
            (*thd_ctx_pp) = NULL;
            ERROR("Connect to redisServer failed : %s:%d",pool_ctx->ip_buf, pool_ctx->port);
        }
        redisEnableKeepAlive(thd_ctx->conn);  //keepalive
        (*thd_ctx_pp) = thd_ctx;
        INFO("Connect to redisServer success : %s:%d",pool_ctx->ip_buf, pool_ctx->port);
    }
    return NULL;
}

/**
** 销毁线程上下文数据
**/
static void* sync_thd_ctx_fini_func(void * pool_ctx_p, void* thd_ctx_p){
    pool_context_t * pool_ctx = (pool_context_t *)pool_ctx_p;
    thread_context_t * thd_ctx = (thread_context_t *)thd_ctx_p;
    redisFree(thd_ctx->conn);
    free(thd_ctx);
    return NULL;
}

/**
** 同步任务函数
**/
static void* sync_routine(void * pool_ctx_p, void ** thd_ctx_pp, void * arg){
    pool_context_t * pool_ctx = (pool_context_t *)pool_ctx_p;
    thread_context_t * thd_ctx = (thread_context_t *)(*thd_ctx_pp);
    char * command = (char *)arg;
    INFO("execute command to %s:%d @ [%s]", pool_ctx->ip_buf, pool_ctx->port, command);
    redisContext *conn = thd_ctx->conn;
    redisReply * reply = NULL;
    int retry = 2;
    int result = 0;
    while(retry-->0 && !result){
        reply = (redisReply*)redisCommand(conn, command); 
        if(NULL == reply){
            ERROR("execute command failed(hard) to %s:%d @ [%s], error : %s", pool_ctx->ip_buf, pool_ctx->port, command, conn->errstr);
            if(retry>0){
                int ret = redisReconnect(thd_ctx->conn);
                if(!(ret == REDIS_OK)){
                    ERROR("ReConnect to redisServer failed : %s:%d",pool_ctx->ip_buf, pool_ctx->port);
                }
                INFO("ReConnect to redisServer success : %s:%d",pool_ctx->ip_buf, pool_ctx->port);
                continue;
            }else{
                break;
            }
        }else{
            switch(reply->type){
                case REDIS_REPLY_STRING:{
                    result = 1;
                    break;
                }
                case REDIS_REPLY_ARRAY:{
                    result = 1;
                    break;
                }
                case REDIS_REPLY_INTEGER:{
                    result = 1;
                    break;
                }
                case REDIS_REPLY_NIL:{
                    //DO NOTHING
                    ERROR("execute command failed(soft) to %s:%d @ [%s], error : %s", pool_ctx->ip_buf, pool_ctx->port, command, conn->errstr);
                    break;
                }
                case REDIS_REPLY_STATUS:{
                    result = (strcasecmp(reply->str,"OK") == 0)?1:0;
                    break;
                }
                case REDIS_REPLY_ERROR:{
                    //DO NOTHING
                    ERROR("execute command failed(soft) to %s:%d @ [%s], error : %s", pool_ctx->ip_buf, pool_ctx->port, command, conn->errstr);
                    break;
                }
                default:{
                    //DO NOTHING
                    break;
                }
            }
            freeReplyObject(reply);
        }
    }
    if(result){
        INFO("execute command success to %s:%d @ [%s]", pool_ctx->ip_buf, pool_ctx->port, command);
    }else{
        ERROR("execute command failed to %s:%d @ [%s]", pool_ctx->ip_buf, pool_ctx->port, command);
    }
    free(command);  //free memory
    return NULL;
}


/**
** 初始化
**/
int sync_init(UT_array *sync_nodes_array, int thread_num, int param_timeout_secs){
    int len = utarray_len(sync_nodes_array);
    if(!len){
        return -1;
    }
    pool_num = len;
    timeout_secs = param_timeout_secs;
    timeout_tv.tv_sec = param_timeout_secs;
    timeout_tv.tv_usec = 0;
    pcxt = (pool_context_t *)calloc(len,sizeof(pool_context_t));
    if(!pcxt){
        return -1;
    }
    pools = (tpool_t **)calloc(len,sizeof(tpool_t*));
    if(!pools){
        free(pcxt);
        return -1;
    }
    char **p;
    p = NULL;
    int n = 0;
    while ( (p=(char**)utarray_next(sync_nodes_array,p))) {
        if (parse_addr(*p, (pcxt+n)->ip_buf, 20, &(pcxt+n)->port) == 0) {
            tpool_t * pool = *(pools+n);
            pool = tpool_create(thread_num,(pcxt+n),sync_thd_ctx_init_func,sync_thd_ctx_fini_func);
            if(!pool){
                ERROR("tpool_create failed : %s:%d", (pcxt+n)->ip_buf, (pcxt+n)->port);
                //TODO: free memory
                return -1;
            }
            *(pools+n) = pool;
            ++n;
        }
    }
    INFO("sync init success");
    return 0;
}


/**
** 销毁
**/
int sync_fini(){
    int n;
    if(pools){
        for(n=0; n<pool_num; ++n){
            if( *(pools+n) != NULL){
                INFO("destroy pool %s:%d",(pcxt+n)->ip_buf, (pcxt+n)->port);
                tpool_destroy(*(pools+n));
            }
        }
    }
    if(pcxt){
        free(pcxt);
    }
    INFO("sync fini success");
    return 0;
}


/**
** 处理redis命令
** 需要对所有的配置节点都执行这个命令
**/
int sync_redis_command(const char * param_command){
    char * command = (char *)calloc(MSG_MAX_LEN,sizeof(char));
    if(!command){
        ERROR("No avaliable memory!");
        return -1;
    }
    snprintf (command, MSG_MAX_LEN, "%s", param_command);
    INFO("handle command : %s", command);
    if(!pool_num){
        ERROR("No avaliable pools!");
        return -1;
    }
    int n;
    if(pools){
        for(n=0; n<pool_num; ++n){
            if( *(pools+n) != NULL){
                tpool_add_work(*(pools+n), sync_routine, (void *)command);
            }
        }
    }
    return 0;
}
