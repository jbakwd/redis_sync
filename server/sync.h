#ifndef SYNC_H
#define SYNC_H 1

#include <hiredis/hiredis.h>
#include "utarray.h"



/* 线程池上下文 */
typedef struct pool_context {
    char ip_buf[20];
    int port;
} pool_context_t;


/* 线程上下文 */
typedef struct thread_context {
    redisContext *conn;
} thread_context_t;


/**
** 初始化
**/
int sync_init(UT_array *sync_nodes_array, int thread_num, int timeout_secs);


/**
** 销毁
**/
int sync_fini();


/**
** 处理redis命令
** 需要对所有的配置节点都执行这个命令
**/
int sync_redis_command(const char * param_command);


#endif
